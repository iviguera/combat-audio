# combat-audio
Module for [FVTT](https://foundryvtt.com/) v0.7.6

Create and play a configured list automatically when combat starts and restores the previous list when finish.

![Sample Video](samples/demo-combat-audio.mp4)

### Planned Features
- [ ] Setting panel to manage playlists.
- [ ] Stop sounds and restore them just like playlists.
