## [0.0.4] - 2020-11-10
### Added
- Settings parametrization
### Changed
- Upgrade FVTT compatibility version 0.7.5 to 0.7.6
- Bugfix when initialize combatAudio fields.
## [0.0.2] - 2020-11-04
### Added
- Create playlist function
### Changed
- Changelog updated.
- First playlist only plays during combat.
- Second playlist only plays when all enemies are defeated.
## [0.0.1] - 2020-10-30
### Added
- FVTT hooks to manage changes in combat tracker and initialize default playlists.


[0.0.4]: https://gitlab.com/iviguera/combat-audio/-/releases/0.0.4
[0.0.2]: https://gitlab.com/iviguera/combat-audio/-/releases/0.0.2
[0.0.1]: https://gitlab.com/iviguera/combat-audio/-/releases/0.0.1