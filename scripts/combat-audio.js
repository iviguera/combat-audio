const combatPlaylistName = 'combat';
const endCombatPlaylistName = 'enemies_clear';
const moduleName = 'combat-audio';
const combatPlaylistSetting = 'combatPlaylistName';
const endCombatPlaylistSetting = 'endCombatPlaylistName';

//init variable
Hooks.on("init", function () {
    game.combatAudio = {};
    initSettings();
});
//when game is ready init values
Hooks.on("ready", async function () {
    let combatPlaylist = game.settings.get(moduleName, combatPlaylistSetting);
    let endCombatPlaylist = game.settings.get(moduleName, endCombatPlaylistSetting);
    game.combatAudio = {
        "actualPlaylist": null,
        "combatPlaylist": await createPlaylist(combatPlaylist),
        "endCombatPlaylist": await createPlaylist(endCombatPlaylist),
        "previousPlaylists": [],
        "enemiesCounter": 0,
        "status": 0
    };
});

Hooks.on("preCreateCombat", function (combat) {
    console.log("preCreateCombat");
});
Hooks.on("createCombat", function (combat) {
    console.log("createCombat");
});
Hooks.on("updateCombat", function (combat, update, options, userId) {
    console.log("updateCombat");
    if (update.round > 0) {
        let combatAudio = game.combatAudio;
        if (combatAudio.enemiesCounter === 0  && combatAudio.status > 0) { //combat is finished
            endCombat(combatAudio);
        }
    }
});
Hooks.on("preDeleteCombat", function (combat) {
    console.log("preDeleteCombat");
});
Hooks.on("deleteCombat", function (combat) {
    console.log("deleteCombat");
    stopEncounter(game.combatAudio);
});

Hooks.on("createCombatant", function (combat, combatant, options, userId) {
    console.log("createCombatant");
    let token = canvas.tokens.placeables.find(t => t.id === combatant.tokenId);
    if (!token.actor.hasPlayerOwner) { //enemy npc
        let combatAudio = game.combatAudio;
        combatAudio.enemiesCounter++;
        if (combatAudio.enemiesCounter === 1) {
            startEncounter(combatAudio);
        }
    }
});
Hooks.on("updateCombatant", function (combat, combatant, options) {
    console.log("updateCombatant");
    if (!combatant.hasPlayerOwner && options.hasOwnProperty('defeated') && options.defeated) {
        let combatAudio = game.combatAudio;
        combatAudio.enemiesCounter--;
        if (combatAudio.enemiesCounter === 0) {
            endCombat(combatAudio);
        }
    }
});

function startEncounter(combatAudio) {
    if (combatAudio.status === 1) {
        return;
    }
    combatAudio.status = 1

    const playlists = game.playlists;
    for (const playList of playlists.playing) {
        _stopTracks(playList);
        combatAudio.previousPlaylists.push(playList.id)
    }
    return _assignToActualAndPlayPlaylist(combatAudio, combatAudio.combatPlaylist);
}

function endCombat(combatAudio) {
    if(combatAudio.status === 2) {
        return;
    }
    combatAudio.status = 2;

    _stopActualPlaylist(combatAudio);

    return _assignToActualAndPlayPlaylist(combatAudio, combatAudio.endCombatPlaylist);
}

function stopEncounter(combatAudio) {
    if(combatAudio.status === 0) {
        return;
    }

    _stopActualPlaylist(combatAudio);

    combatAudio.actualPlaylist = null;

    const playlists = game.playlists;
    for (const playListId of combatAudio.previousPlaylists) {
        _playTracks(playlists.entities.find(p => p.id === playListId));
    }

    combatAudio.enemiesCounter = 0;
    combatAudio.status = 0;
    combatAudio.previousPlaylists = [];
}
function _assignToActualAndPlayPlaylist(combatAudio, playlist) {
    combatAudio.actualPlaylist = playlist;
    return _playTracks(playlist);
}
function _stopActualPlaylist(combatAudio) {
    if(combatAudio.actualPlaylist !== null) {
        _stopTracks(combatAudio.actualPlaylist).then(ui.playlists.render());
    }
}
async function _playTracks(playlist) {
    await playlist.playAll().then(ui.playlists.render());
}
async function _stopTracks(playlist) {
    await playlist.stopAll().then(ui.playlists.render());
}
function _getPlaylist(playlists, name) {
    return playlists.entities.find(p => p.name === name)
}
async function createPlaylist(name) {
    const playlists = game.playlists;
    let combatPlaylist = _getPlaylist(playlists, name);
    if(!combatPlaylist) {
        combatPlaylist = await Playlist.create({"name": name});
    }
    return combatPlaylist;
}

function initSettings() {
    game.settings.register(moduleName, combatPlaylistSetting, {
        name: "Combat Playlist Name",
        hint: "Playlist name to play when combat starts.",
        scope: "world",
        config: true,
        default: combatPlaylistName,
        type: String,
        onChange: async (value) => {
            if(game.combatAudio) {
                game.combatAudio.combatPlaylist = await createPlaylist(value);
            }
        }
    });

    game.settings.register(moduleName, endCombatPlaylistSetting, {
        name: "End Combat Playlist Name",
        hint: "Playlist name to play when enemies are defeated.",
        scope: "world",
        config: true,
        default: endCombatPlaylistName,
        type: String,
        onChange: async (value) => {
            if(game.combatAudio) {
                game.combatAudio.endCombatPlaylist = await createPlaylist(value);
            }
        }
    });
}